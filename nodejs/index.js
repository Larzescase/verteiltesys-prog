const express = require('express')
const app = express();
const db = require("./database")
const bodyParser = require('body-parser');

let names = ['Jannik','Lars']

app.use(express.json());
app.use(express.urlencoded({
    extended:false
}));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
  });

app.use(bodyParser.urlencoded({ extended: true }));

app.get("/",(req, res) =>{
    db.query("SELECT * FROM nachrichten", (err, result, field) =>{
        if(err) throw err;

        res.json(result)
    })
    
})

app.post("/api/messages",(req, res) =>{
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    console.log(req.body.name)
    console.log(req.body.nachricht)
    db.query("INSERT INTO nachrichten (username, nachricht) VALUES (?,?)",[req.body.name,req.body.nachricht],
    (err, result, req) =>{
        if(err) throw err;

        res.sendStatus(200)
    })
})

app.listen(8080, () =>{
    console.log("Server is running on Port 8080")
})